import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Overview from "../components/overview"
import Card from "../components/card"

const BasePage = () => (
    <Layout>
        <SEO title="Grund att stå på" />
        <Overview prefix="1" title="Grund att stå på" step1="Principprogram och sakpolitik" step2="Politik i Sverige" step3="IT-säkerhet" />
        <section className="section is-fullheight">
            <div className="columns">
                <div className="column is-third">
                    <Card title="Principprogram och sakpolitik" to="base/principles"/>
                </div>
                <div className="column is-third">
                    <Card title="Politik i Sverige"/>
                </div>
                <div className="column is-third">
                    <Card title="IT-säkerhet"/>
                </div>
            </div>
        </section>
    </Layout>
)

export default BasePage;
