import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Hero from "../components/hero"
import Module from "../components/module"

const IndexPage = () => (
  <Layout>
    <SEO title="Väkommen" />
    <Hero url="#modules" title="Välkommen ombord!" body="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."/>
    <section id="modules" className="section">
      <div className="container">
        <div className="columns">
          <div className="column is-half">
            <Module href="/base" title="Grund att stå på" step1="Principprogram och sakpolitik" step2="Politik i Sverige" step3="IT-säkerhet"></Module>
          </div>
          <div className="column is-half">
            <Module href="" title="Vad vill vi uppnå?" step1="Vision" step2="Mission" step3="Utmaningar"></Module>
          </div>
        </div>
        <div className="columns">
          <div className="column is-half">
            <Module href="" title="Organisationen" step1="Hur är vi organsiserade" step2="Piratpartiets historia" step3="Principer"></Module>
          </div>
          <div className="column is-half">
            <Module href="" title="Hur kan du engagera dig?" step1="Olika nivåer av engagemang" step2="Olika uppdrag" step3="Något mer"></Module>
          </div>
        </div>
      </div>
    </section>
  </Layout>
)

export default IndexPage
