import React from "react"

import { Link } from "gatsby";
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import OverviewSteps from "../../components/overviewsteps"

const PrinciplesPage = () => (
    <Layout>
        <SEO title="Principprogram och Sakpolitik" />
        <OverviewSteps
            title="Principprogram och Sakpolitik"
            step1="Modern informationsteknik öppnar fantastiska möjligheter"
            step1url="base/principles/steps#1"
            step2="Starka krafter vill lägga hinder för utvecklingen"
            step2url="base/principles/steps#2"
            step3="Piratpartiets uppgift och grundsatser"
            step3url="base/principles/steps#3"
            step4="Piratpartiets vision – en tro på framtiden"
            step4url="base/principles/steps#4"
        />
    </Layout>
)

export default PrinciplesPage;
