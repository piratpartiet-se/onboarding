import React from "react"
import ArrowDown from "./arrowdown"

const Hero = ({ title, body, id, url}) => (
    <section id={id} className="hero is-primary is-fullheight">
        <div className="hero-body hero-flex">
            <div className="container hero-container">
                <p className="title is-size-1 has-text-weight-light">
                    { title }
                </p>
                <p>
                    { body }
                </p>
            </div>
            <ArrowDown href={url} />
        </div>
    </section>
)

export default Hero;
