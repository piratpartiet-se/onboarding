import PropTypes from "prop-types"
import React from "react"
import Helmet from 'react-helmet'

const Header = ({ siteTitle }) => (
  <header>
    <Helmet/>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: `Piratpartiet`,
}

export default Header
