import React from "react"

const Overview = ({ title, prefix, step1, step2, step3, id, url}) => (
    <section id={id} className="hero is-primary is-fullheight">
        <div className="hero-body">
            <div className="container">
                <h1 className="title is-size-1">
                    {prefix}. { title }
                </h1>
                <div id="line"></div>
                <p className="buttons are-large list-step">
                    <button className="button is-info">
                        <span className="icon is-large">
                            <i class="fas fa-play"></i>
                        </span>
                    </button>
                    <h2 className="title is-2 has-text-weight-light">
                        { step1 }
                    </h2>
                </p>
                <p className="buttons are-large list-step">
                    <button className="button is-info">
                        <span className="icon is-large">
                            <i class="fas fa-play"></i>
                        </span>
                    </button>
                    <h2 className="title is-2 has-text-weight-light">
                        { step2 }
                    </h2>
                </p>
                <p className="buttons are-large list-step">
                    <button className="button is-info">
                        <span className="icon is-large">
                            <i class="fas fa-play"></i>
                        </span>
                    </button>
                    <h2 className="title is-2 has-text-weight-light">
                        { step3 }
                    </h2>
                </p>
            </div>
        </div>
    </section>
)

export default Overview;
