import {Link} from "gatsby"
import React from "react"

class Module extends React.Component {
    render() {
        return (
            <Link className="module box" to={this.props.href}>
                <div className="content">
                    <h1>{this.props.title}</h1>
                    <p>{this.props.step1}</p>
                    <p>{this.props.step2}</p>
                    <p>{this.props.step3}</p>
                </div>
            </Link>
        )
    }
}

export default Module;
