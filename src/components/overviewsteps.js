import React from "react"
import { Link } from "gatsby";

const OverviewSteps = ({ title, step1, step1url, step2, step2url, step3, step3url, step4, step4url, id }) => (
    <section id={id} className="hero is-primary is-fullheight">
        <div className="hero-body">
            <div className="container">
                <h1 className="title is-size-1">
                    {title}
                </h1>
                <div id="line"></div>
                <div className="box">
                    <Link to={step1url}>
                        <h2 className="title is-2 has-text-weight-light color has-text-black">
                            {step1}
                        </h2>
                    </Link>
                </div>
                <div className="box">
                    <Link to={step2url}>
                        <h2 className="title is-2 has-text-weight-light color has-text-black">
                            {step2}
                        </h2>
                    </Link>
                </div>
                <div className="box">
                    <Link to={step3url}>
                        <h2 className="title is-2 has-text-weight-light color has-text-black">
                            {step3}
                        </h2>
                    </Link>
                </div>
                <div className="box">
                    <Link to={step4url}>
                        <h2 className="title is-2 has-text-weight-light color has-text-black">
                            {step4}
                        </h2>
                    </Link>
                </div>
            </div>
        </div>
    </section>
)

export default OverviewSteps;
