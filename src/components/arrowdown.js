import React from "react"
import { Link } from "gatsby";

class ArrowDown extends React.Component {
    handleClick = () => {
        document.location.href = this.props.href;
    }

    render() {
        return (
            <a className="arrow-down" onClick={this.handleClick}>
                <i className="fas fa-angle-double-down"></i>
            </a>
        )
    }
}

export default ArrowDown;
