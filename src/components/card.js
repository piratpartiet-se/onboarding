import React from "react"
import { Link } from "gatsby"

const Card = ({ title, to, children }) => (
    <Link to={to}>
        <div className="card">
            <div className="card-image">
                <figure className="image is-4by3">
                    <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image" />
                </figure>
            </div>
            <div className="card-content">
                <div className="media">
                    <div className="media-content">
                        <p className="title is-4">{title}</p>
                    </div>
                </div>
                <div className="content">
                    {children}
                </div>
            </div>
        </div>
    </Link>
)

export default Card;