# ![Piratpartiet][pp_logo]

Onboarding sida för Piratpartiet. Detta är tänkt att vara ett hjälpmedel för nya medlemmar att bli mer bekant med partiets principer och politiker men även att få en insyn till hur organisationen är uppbyggd och hur man kan hjälpa till. Denna sida är byggd med [`gatsby`](https://www.gatsbyjs.org/) som kan installeras med följande kommando: `npm install -g gatsby-cli`

_Vill du hjälpa till? Gå med i partiets [officiella chatt](https://chat.piratpartiet.se) genom att kontakta [Mattias Rubenson](mailto:mattias.rubenson@piratpartiet.se) eller [Jakob Sinclair](mailto:jakob.sinclair@piratpartiet.se). Där kan du få hjälp och idéer på vad som behöver göras!_

## 🚀 Quick start

1.  **Klona detta repo**

    Använd [`git`](https://git-scm.com/) för att få ned koden på din dator.

    ```shell
    git clone https://gitlab.com/piratpartiet-se/onboarding
    ```

2.  **Starta utvecklingsmiljön**

    Navigera in i den nya katalogen `onboarding` och starta [`gatsby`](https://www.gatsbyjs.org/).

    ```shell
    cd onboarding/
    gatsby develop
    ```

3.  **Öppna koden och börja hacka!**

    Onboarding sidan körs nu på `http://localhost:8000`!

    Öppna `onboarding` katalogen i valfri kodredigera och testa att ändra filen `src/pages/index.js`. Spara dina ändringar så kommer webläsaren att uppdatera i realtid!

## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you will see on the front-end of your site (what you see in the browser) such as your site header or a page template. `src` is a convention for “source code”.

3.  **`.gitignore`**: This file tells git which files it should not track / not maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for [Prettier](https://prettier.io/). Prettier is a tool to help keep the formatting of your code consistent.

5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/) (if any). These allow customization/extension of default Gatsby settings affecting the browser.

6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information about your site (metadata) like the site title and description, which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).

7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of the [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/) (if any). These allow customization of default Gatsby settings affecting server-side rendering.

9.  **`LICENSE`**: Gatsby is licensed under the MIT license.

10. **`package-lock.json`** (See `package.json` below, first). This is an automatically generated file based on the exact versions of your npm dependencies that were installed for your project. **(You won’t change this file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes things like metadata (the project’s name, author, etc). This manifest is how npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about your project.

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives [on the website](https://www.gatsbyjs.org/). Here are some places to start:

- **For most developers, we recommend starting with our [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).** It starts with zero assumptions about your level of ability and walks through every step of the process.

- **To dive straight into code samples, head [to our documentation](https://www.gatsbyjs.org/docs/).** In particular, check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections in the sidebar.

<!-- AUTO-GENERATED-CONTENT:END -->
[pp_logo]: https://piratpartiet.se/wp-content/themes/pp_www/img/pp_logga_liggande_lila_svart.svg